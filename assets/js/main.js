window.onload = function() {
	//Zoveel mogelijk const gebruiken, anders let!
	//dom elements
	const pixels = document.getElementsByClassName('js--pixel');
	const spheres = document.getElementsByClassName("js--sphere");
	const cursor = document.getElementById("js--cursor");
	let brush = "black";


	//Hier gebruiken we let omdat we i meerdere malen assignen
	for(let i = 0; i < pixels.length; i++){
			//Voor elke plane in de lijst voegen we een onmouseenter toe, es6 functie geschreven.
			pixels[i].onmouseenter = (event) => {
				pixels[i].setAttribute('color',brush);

			}
	}

	for(let i = 0; i < spheres.length; i++){
		 spheres[i].onmouseenter = (event) =>{
			 brush = spheres[i].getAttribute('color');
			 cursor.setAttribute('color',brush);
		 }
	}





}
